class Ticket {
  String? id;
  String? userId;
  String? firstName;
  String? lastName;
  String? name;
  String? date;
  String? time;
  String? venueName;

  Ticket(
      {this.id,
      this.userId,
      this.firstName,
      this.lastName,
      this.name,
      this.date,
      this.time,
      this.venueName});

  Ticket.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    userId = json['userId'];
    firstName = json['firstName'];
    lastName = json['lastName'];
    name = json['name'];
    date = json['date'];
    time = json['time'];
    venueName = json['venueName'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['userId'] = this.userId;
    data['firstName'] = this.firstName;
    data['lastName'] = this.lastName;
    data['name'] = this.name;
    data['date'] = this.date;
    data['time'] = this.time;
    data['venueName'] = this.venueName;
    return data;
  }
}