class Event {
  String? id;
  String? name;
  String? date;
  String? time;
  String? lat;
  String? lng;
  String? venueName;
  String? price;

  Event(
      {this.id,
      this.name,
      this.date,
      this.time,
      this.lat,
      this.lng,
      this.venueName,
      this.price});

  Event.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    date = json['date'];
    time = json['time'];
    lat = json['lat'];
    lng = json['lng'];
    venueName = json['venueName'];
    price = json['price'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['date'] = this.date;
    data['time'] = this.time;
    data['lat'] = this.lat;
    data['lng'] = this.lng;
    data['venueName'] = this.venueName;
    data['price'] = this.price;
    return data;
  }
}