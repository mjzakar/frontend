// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'dart:convert';

class LoginDTO {
  String userId;
  String accessToken;
  String refreshToken;
  
  LoginDTO({
    required this.userId,
    required this.accessToken,
    required this.refreshToken
  });

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'userId': userId,
      'accessToken': accessToken,
      'refreshToken': refreshToken,
    };
  }

  factory LoginDTO.fromMap(Map<String, dynamic> map) {
    return LoginDTO(
      userId: map['userId'] as String,
      accessToken: map['accessToken'] as String,
      refreshToken: map['refreshToken'] as String,
      
    );
  }

  String toJson() => json.encode(toMap());

  factory LoginDTO.fromJson(String source) =>
      LoginDTO.fromMap(json.decode(source) as Map<String, dynamic>);
}
