// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:flutter/material.dart';
import 'package:frontend/screens/admin_page/adminpage.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'package:frontend/screens/home_page/home_page.dart';
import 'package:frontend/screens/login/login.dart';
import 'package:frontend/screens/map_page/map.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  SharedPreferences prefs = await SharedPreferences.getInstance();
  bool loggedIn = prefs.getBool("loggedIn") ?? false;
  bool isAdmin = prefs.getBool("isAdmin") ?? false;
  runApp(MyApp(
    loggedIn: loggedIn,
    isAdmin: isAdmin,
  ));
}

class MyApp extends StatelessWidget {
  bool loggedIn;
  bool isAdmin;
  MyApp({
    Key? key,
    required this.loggedIn,
    required this.isAdmin,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
            primarySwatch: Colors.blue,
            fontFamily: 'PT Sans',
            textTheme: Theme.of(context).textTheme.apply(
                  bodyColor: Colors.white,
                  displayColor: Colors.white,
                )),
        home: loggedIn ? isAdmin ? AdminPage() : MapPage() : Login());
  }
}
