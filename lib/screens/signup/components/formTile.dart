// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class FormTile extends StatefulWidget {
  Icon? prefixIcon;
  String labelText;
  String hintText;
  int? minLines;
  bool obscure;
  var validator;
  var controller;
  FormTile({
    Key? key,
    this.prefixIcon,
    required this.controller,
    required this.obscure,
    required this.labelText,
    required this.hintText,
    this.minLines = 1,
    this.validator,
  }) : super(key: key);

  @override
  State<FormTile> createState() => _FormTileState();
}

class _FormTileState extends State<FormTile> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
       
            Padding(
              padding: const EdgeInsets.only(left: 3),
              child: TextFormField(
                obscureText: widget.obscure,
                controller: widget.controller,
                  style: TextStyle(color: Colors.black),
                  validator: widget.validator,
                  minLines: widget.minLines,
                  maxLines: widget.minLines,
                  decoration: InputDecoration(
                    filled: true,
                    fillColor: Colors.grey.shade100,
                    contentPadding: EdgeInsets.all(10),
                    prefixIconConstraints: widget.prefixIcon != null
                        ? BoxConstraints(maxWidth: 50, minWidth: 50)
                        : BoxConstraints(maxWidth: 10),
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(12),
                        borderSide: BorderSide(width: 40)),
                    prefixIcon: widget.prefixIcon ?? Container(),
                    hintText: widget.hintText,
                  )),
            )
          ]),
    );
  }
}
