import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:frontend/models/LoginDTO.dart';
import 'package:frontend/models/user.dart';
import 'package:frontend/screens/home_page/home_page.dart';
import 'package:frontend/screens/login/login.dart';
import 'package:frontend/screens/map_page/map.dart';
import 'package:frontend/screens/signup/components/formTile.dart';
import 'package:frontend/data/api_service.dart' as API;
import 'package:shared_preferences/shared_preferences.dart';

class Body extends StatefulWidget {
  Body({
    Key? key,
  }) : super(key: key);

  @override
  State<Body> createState() => _BodyState();
}

class _BodyState extends State<Body> {
  var firstNameController = TextEditingController();

  var lastNameController = TextEditingController();

  var emailController = TextEditingController();

  var passwordController = TextEditingController();

  final loginFormKey = GlobalKey<FormState>();
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    firstNameController.text = 'Jay';
    lastNameController.text = 'Grace';
    emailController.text = 'mjzakar@gmail.com';
    passwordController.text = '12341234';
  }

  @override
  Widget build(BuildContext context) {
    var mediaQuery = MediaQuery.of(context).size;

    return SafeArea(
      child: Padding(
        padding: const EdgeInsets.all(40),
        child: Center(
          child: SizedBox(
            height: mediaQuery.height * 0.77,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                SizedBox(
                  height: mediaQuery.height * 0.15,
                  child: Image.asset(
                    'assets/images/logo.png',
                  ),
                ),
                SizedBox(
                  height: mediaQuery.height * 0.02,
                ),
                Text(
                  "Sign Up on Ticketify",
                  textAlign: TextAlign.center,
                  style: TextStyle(fontFamily: "PT Sans", fontSize: 22),
                ),
                Form(
                  key: loginFormKey,
                  child: Expanded(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        FormTile(
                          controller: firstNameController,
                          hintText: 'First Name',
                          labelText: 'First Name',
                          obscure: false,
                          prefixIcon: Icon(Icons.person_2_outlined),
                          validator: (value) {
                            if (value == '') {
                              return 'First name can\'t be empty!';
                            } else {
                              return null;
                            }
                          },
                        ),
                        SizedBox(
                          height: mediaQuery.height * 0.013,
                        ),
                        FormTile(
                          controller: lastNameController,
                          hintText: 'Last Name',
                          labelText: 'Last Name',
                          obscure: false,
                          prefixIcon: Icon(Icons.person_2_outlined),
                          validator: (value) {
                            if (value == '') {
                              return 'Last name can\'t be empty!';
                            } else {
                              return null;
                            }
                          },
                        ),
                        SizedBox(
                          height: mediaQuery.height * 0.013,
                        ),
                        FormTile(
                          controller: emailController,
                          obscure: false,
                          hintText: 'Email',
                          labelText: "Enter Your Email:",
                          prefixIcon: Icon(Icons.email_outlined),
                          validator: (value) {
                            if (value == null ||
                                !RegExp(r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
                                    .hasMatch(value)) {
                              return 'Enter a valid email';
                            } else {
                              return null;
                            }
                          },
                        ),
                        SizedBox(
                          height: mediaQuery.height * 0.013,
                        ),
                        FormTile(
                          controller: passwordController,
                          hintText: 'Password',
                          labelText: 'Password',
                          obscure: true,
                          prefixIcon: Icon(Icons.lock_outline),
                          validator: (value) =>
                              value == null || value.length < 8
                                  ? 'Password can\'t be less than 8 characters'
                                  : null,
                        ),
                        SizedBox(
                          height: mediaQuery.height * 0.013,
                        ),
                      ],
                    ),
                  ),
                ),
                ElevatedButton(
                    onPressed: () async {
                      if (loginFormKey.currentState!.validate()) {
                        User user = User(
                            firstName: firstNameController.text,
                            lastName: lastNameController.text,
                            email: emailController.text,
                            password: passwordController.text,
                            role: "user");
                        LoginDTO? loginDetails =
                            await API.ApiService().signup(user);
                        if (loginDetails != null) {
                          SharedPreferences prefs =
                              await SharedPreferences.getInstance();
                          prefs.setBool('loggedIn', true);
                          prefs.setString('userId', loginDetails.userId);
                          prefs.setString('access', loginDetails.accessToken);
                          prefs.setString('refresh', loginDetails.refreshToken);
                          prefs.setBool("isAdmin", false);
                          Navigator.pushReplacement(
                              context,
                              MaterialPageRoute(
                                  builder: ((context) => MapPage())));
                        } else
                          showDialog(
                            context: context,
                            builder: (context) => Text('nuh uh!'),
                          );
                      }
                    },
                    style: ElevatedButton.styleFrom(
                      backgroundColor: Color.fromRGBO(230, 68, 0, 1),
                      textStyle: const TextStyle(fontSize: 16),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(25),
                      ),
                    ),
                    child: const Text('Sign Up')),
                TextButton(
                    onPressed: () {
                      Navigator.pushReplacement(context,
                          MaterialPageRoute(builder: ((context) => Login())));
                    },
                    child: const Text(
                      'Already have an account? Login now',
                      style: TextStyle(color: Colors.blueGrey, fontSize: 15),
                    ))
              ],
            ),
          ),
        ),
      ),
    );
  }
}
