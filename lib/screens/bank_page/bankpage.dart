// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:flutter/material.dart';

import 'package:frontend/screens/bank_page/components/body.dart';

import '../../models/Event.dart';

class BankPage extends StatelessWidget {
  Event event;
  BankPage({
    Key? key,
    required this.event,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color(0xff00ADB5),
        title: Text(
          'Payment Page',
          style: TextStyle(color: Color(0xffEEEEEE)),
        ),
      ),
  
      resizeToAvoidBottomInset: false,
      body: Container(
          decoration: BoxDecoration(
              gradient: LinearGradient(
                  colors: [Color(0xff222831), Color(0xFF393E46)],
                  begin: Alignment.topLeft,
                  end: Alignment.bottomRight)),
          child: Body(event: event)),
    );
  }
}
