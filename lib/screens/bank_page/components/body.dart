// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:flutter/material.dart';
import 'package:frontend/data/api_service.dart';
import 'package:frontend/models/Ticket.dart';
import 'package:frontend/screens/map_page/map.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../../models/Event.dart';

class Body extends StatefulWidget {
  Event event;
  Body({
    Key? key,
    required this.event,
  }) : super(key: key);

  @override
  State<Body> createState() => _BodyState();
}

class _BodyState extends State<Body> {
  final TextEditingController cardNumberController = TextEditingController();
  final TextEditingController expMonthController = TextEditingController();
  final TextEditingController expYearController = TextEditingController();
  final TextEditingController cvcController = TextEditingController();
  
  @override
  void initState() {
    cardNumberController.text = "4716 1415 8024 8283";
    expMonthController.text = "12";
    expYearController.text = "2022";
    cvcController.text = "123";
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 140, horizontal: 30),
      child: Container(
        padding: EdgeInsets.all(20),
        decoration: BoxDecoration(borderRadius: BorderRadius.circular(20), color: Color.fromARGB(255, 72, 80, 92)),
        child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                'Card Number:',
                style: TextStyle(fontSize: 18),
              ),
              TextField(
                controller: cardNumberController,
                keyboardType: TextInputType.number,
                decoration: InputDecoration(
                  hintText: 'Enter card number',
                  hintStyle: TextStyle(color: Color(0xffEEEEEE))
                ),
              ),
              SizedBox(height: 16),
              Text(
                'Expiration Date:',
                style: TextStyle(fontSize: 18),
              ),
              Row(
                children: [
                  Expanded(
                    child: TextField(
                      controller: expMonthController,
                      keyboardType: TextInputType.number,
                      decoration: InputDecoration(
                        hintText: 'MM',
                        hintStyle: TextStyle(color: Color(0xffEEEEEE))
                      ),
                    ),
                  ),
                  SizedBox(width: 16),
                  Expanded(
                    child: TextField(
                      controller: expYearController,
                      keyboardType: TextInputType.number,
                      decoration: InputDecoration(
                        hintText: 'YY',
                        hintStyle: TextStyle(color: Color(0xffEEEEEE))
                      ),
                    ),
                  ),
                ],
              ),
              SizedBox(height: 16),
              Text(
                'CVC:',
                style: TextStyle(fontSize: 18),
              ),
              TextField(
                controller: cvcController,
                keyboardType: TextInputType.number,
                decoration: InputDecoration(
                  hintText: 'Enter CVC',
                  hintStyle: TextStyle(color: Color(0xffEEEEEE))
                ),
              ),
              SizedBox(height: 100),
              
              Row(mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  ElevatedButton(
                    style: ElevatedButton.styleFrom(backgroundColor: Color(0xff00ADB5)),
                    onPressed: () {
                      _showPaymentResult(context, widget.event);
                    },
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 12),
                      child: Text('Purchase', style: TextStyle(fontSize: 20,color: Color(0xffEEEEEE)),),
                    ),
                  ),
                ],
              ),
            ],
          ),
      ),
    );
  }

  void _showPaymentResult(BuildContext context, Event event) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(backgroundColor: Color(0xff393E46),
          title: Text('Payment Result'),
          content: Text('Payment Successful'),
          actions: [
            TextButton(
              onPressed: () async {
                SharedPreferences sharedPrefs =
                    await SharedPreferences.getInstance();
                var userId = sharedPrefs.getString("userId");
                var accessToken = sharedPrefs.getString('access');
                var user = await ApiService()
                    .getUserDetails(accessToken ?? "", userId ?? "");
                Ticket ticket = Ticket(
                    date: event.date,
                    firstName: user!.firstName,
                    lastName: user.lastName,
                    name: event.name,
                    time: event.time,
                    
                    userId: user.id,
                    venueName: event.venueName);
                await ApiService().addTicket(ticket);
                Navigator.pushReplacement(context,
                    MaterialPageRoute(builder: ((context) => MapPage())));
              },
              child: Text('OK', style: TextStyle(color: Color(0xffEEEEEE), fontSize: 16)),
            ),
          ],
        );
      },
    );
  }
}

