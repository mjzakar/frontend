import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:frontend/models/LoginDTO.dart';
import 'package:frontend/screens/admin_page/adminpage.dart';
import 'package:frontend/screens/home_page/home_page.dart';
import 'package:frontend/screens/map_page/map.dart';
import 'package:frontend/screens/signup/signup.dart';
import 'package:frontend/data/api_service.dart' as API;
import 'package:shared_preferences/shared_preferences.dart';

import '../../signup/components/formTile.dart';

class Body extends StatefulWidget {
  Body({
    Key? key,
  }) : super(key: key);

  @override
  State<Body> createState() => _BodyState();
}

class _BodyState extends State<Body> {
  var emailController = TextEditingController();

  var passwordController = TextEditingController();

  final loginFormKey = GlobalKey<FormState>();

  @override
  void initState() {
    emailController.text = 'mjzakar@gmail.com';
    passwordController.text = '12341234';
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var mediaQuery = MediaQuery.of(context).size;
    return SafeArea(
      child: Padding(
        padding: const EdgeInsets.all(50),
        child: Center(
          child: SizedBox(
            height: mediaQuery.height * 0.55,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                SizedBox(
                  height: mediaQuery.height * 0.15,
                  child: Image.asset(
                    'assets/images/logo.png',
                  ),
                ),
                SizedBox(
                  height: mediaQuery.height * 0.02,
                ),
                Text(
                  "Login to Ticketify",
                  textAlign: TextAlign.center,
                  style: TextStyle(fontFamily: "PT Sans", fontSize: 22),
                ),
                Form(
                  key: loginFormKey,
                  child: Expanded(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        FormTile(
                          controller: emailController,
                          obscure: false,
                          hintText: 'Email',
                          labelText: "Enter Your Email:",
                          prefixIcon: Icon(Icons.email_outlined),
                          validator: (value) {
                            if (value == null ||
                                !RegExp(r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
                                    .hasMatch(value)) {
                              return 'Enter a valid email';
                            } else {
                              return null;
                            }
                          },
                        ),
                        SizedBox(
                          height: mediaQuery.height * 0.013,
                        ),
                        FormTile(
                          controller: passwordController,
                          hintText: 'Password',
                          labelText: 'Password',
                          obscure: true,
                          prefixIcon: Icon(Icons.lock_outline),
                          validator: (value) =>
                              value == null || value.length < 8
                                  ? 'Password can\'t be less than 8 characters'
                                  : null,
                        ),
                      ],
                    ),
                  ),
                ),
                ElevatedButton(
                    onPressed: () async {
                      if (loginFormKey.currentState!.validate()) {
                        LoginDTO? loginDetails = await API.ApiService().login(
                            emailController.text, passwordController.text);
                        if (loginDetails != null) {
                          SharedPreferences prefs =
                              await SharedPreferences.getInstance();
                          await prefs.setBool('loggedIn', true);
                          await prefs.setString('userId', loginDetails.userId);
                          await prefs.setString(
                              'access', loginDetails.accessToken);
                          await prefs.setString(
                              'refresh', loginDetails.refreshToken);
                          var userDetails = await API.ApiService()
                              .getUserDetails(loginDetails.accessToken,
                                  loginDetails.userId);
                          if (userDetails?.role != null &&
                              userDetails?.role == "admin") {
                            await prefs.setBool("isAdmin", true);
                            Navigator.pushReplacement(
                                context,
                                MaterialPageRoute(
                                    builder: ((context) => AdminPage())));
                          } else {
                            Navigator.pushReplacement(
                                context,
                                MaterialPageRoute(
                                    builder: ((context) => MapPage())));
                          }
                        } else
                          showAlertDialog(context);
                      }
                    },
                    style: ElevatedButton.styleFrom(
                      backgroundColor: Color.fromRGBO(230, 68, 0, 1),
                      textStyle: const TextStyle(fontSize: 16),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(25),
                      ),
                    ),
                    child: const Text('Log In')),
                TextButton(
                    onPressed: () {
                      Navigator.pushReplacement(context,
                          MaterialPageRoute(builder: ((context) => SignUp())));
                    },
                    child: const Text(
                      'Don\'t have an account? Sign Up!',
                      style: TextStyle(color: Colors.blueGrey, fontSize: 15),
                    ))
              ],
            ),
          ),
        ),
      ),
    );
  }

  void showAlertDialog(BuildContext context) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          actionsAlignment: MainAxisAlignment.center,
          backgroundColor: Colors.black,
          title: Text('Authentication Error!'),
          content: Text('Wrong email/password!'),
          actions: [
            TextButton(
              onPressed: () {
                Navigator.of(context).pop();
              },
              child: Text('OK'),
            ),
          ],
        );
      },
    );
  }
}
