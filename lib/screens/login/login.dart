// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:flutter/material.dart';
import 'package:frontend/screens/login/components/body.dart';

class Login extends StatelessWidget {
  Login({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Container(
          decoration: BoxDecoration(
              gradient: LinearGradient(
                  colors: [Color.fromRGBO(37, 61, 100, 1), Color(0xFF191919)],
                  begin: Alignment.topLeft,
                  end: Alignment.bottomRight)),
          child: Body()),
    );
  }
}
