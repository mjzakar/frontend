import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:frontend/data/api_service.dart';
import 'package:frontend/models/Event.dart';
import 'package:latlong2/latlong.dart';

class Body extends StatefulWidget {
  const Body({super.key});

  @override
  State<Body> createState() => _BodyState();
}

class _BodyState extends State<Body> {
  final TextEditingController nameController = TextEditingController();
  final TextEditingController dateController = TextEditingController();
  final TextEditingController timeController = TextEditingController();
  final TextEditingController latlongController = TextEditingController();
  final TextEditingController venueController = TextEditingController();
  final TextEditingController priceController = TextEditingController();

  static const double pointSize = 65;

  final mapController = MapController();

  LatLng? tappedCoords;
  Point<double>? tappedPoint;

  @override
  void initState() {
    super.initState();

    SchedulerBinding.instance
        .addPostFrameCallback((_) => ScaffoldMessenger.of(context).showSnackBar(
              const SnackBar(content: Text('Tap/click to set coordinate')),
            ));
  }

  DateTime selectedDate = DateTime.now();
  TimeOfDay selectedTime = TimeOfDay.now();

  Future<void> _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
          context: context,
          initialDate: selectedDate,
          firstDate: DateTime(2000),
          lastDate: DateTime(2101),
        ) ??
        DateTime.now();
    ;

    if (picked != null && picked != selectedDate)
      setState(() {
        selectedDate = picked;
        dateController.text = picked.toLocal().toString().split(' ')[0];
      });
  }

  Future<void> _selectTime(BuildContext context) async {
    final TimeOfDay picked = await showTimePicker(
          context: context,
          initialTime: selectedTime,
        ) ??
        TimeOfDay.now();

    if (picked != null && picked != selectedTime)
      setState(() {
        selectedTime = picked;
        timeController.text =
            '${selectedTime.hour.toString().padLeft(2, '0')}:${selectedTime.minute.toString().padLeft(2, '0')}:00';
      });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xff222831),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            buildFormTile(
              controller: nameController,
              labelText: 'Event Name',
            ),
            buildFormTile(
              controller: dateController,
              labelText: 'Date',
              suffixIcon: IconButton(
                icon: Icon(Icons.calendar_today, color: Color(0xffEEEEEE)),
                onPressed: () => _selectDate(context),
              ),
              readOnly: true,
            ),
            buildFormTile(
              controller: timeController,
              labelText: 'Time',
              suffixIcon: IconButton(
                icon: Icon(Icons.access_time, color: Color(0xffEEEEEE)),
                onPressed: () => _selectTime(context),
              ),
              readOnly: true,
            ),
            buildFormTile(
              controller: latlongController,
              labelText: 'Coordinates',
              suffixIcon: IconButton(
                icon: Icon(Icons.pin_drop, color: Color(0xffEEEEEE)),
                onPressed: () => _showModal(context),
              ),
            ),
            buildFormTile(
              controller: venueController,
              labelText: 'Venue Name',
            ),
            buildFormTile(
              controller: priceController,
              labelText: 'Price',
            ),
            SizedBox(height: 20),
            ElevatedButton(
              style: ElevatedButton.styleFrom(
                primary: Color(0xff00ADB5), // background color
                onPrimary: Color(0xff222831), // text color
              ),
              onPressed: () async {
                await ApiService().createEvent(Event(
                    date: dateController.text,
                    lat: tappedCoords!.latitude.toString(),
                    lng: tappedCoords!.longitude.toString(),
                    name: nameController.text,
                    price: priceController.text,
                    time: timeController.text,
                    venueName: venueController.text));
                Navigator.pop(context);
              },
              child: Text('Save Event',
                  style: TextStyle(color: Color(0xffEEEEEE))),
            ),
          ],
        ),
      ),
    );
  }

  Widget buildFormTile({
    required TextEditingController controller,
    required String labelText,
    IconButton? suffixIcon,
    bool readOnly = false,
  }) {
    return TextFormField(
      controller: controller,
      readOnly: readOnly,
      style: TextStyle(color: Color(0xffEEEEEE)),
      decoration: InputDecoration(
        labelText: labelText,
        labelStyle: TextStyle(color: Color(0xffEEEEEE)),
        fillColor: Color(0xff393E46),
        filled: true,
        suffixIcon: suffixIcon,
        enabledBorder: OutlineInputBorder(
          borderSide: BorderSide(
            color: Color(0xff00ADB5),
            width: 2.0,
          ),
        ),
        focusedBorder: OutlineInputBorder(
          borderSide: BorderSide(
            color: Color(0xff00ADB5),
            width: 2.0,
          ),
        ),
      ),
    );
  }

  void _showModal(BuildContext context) async {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return Center(
            child: Stack(
              children: [
                FlutterMap(
                  mapController: mapController,
                  options: MapOptions(
                    center: LatLng(36.561128, 52.680570),
                    zoom: 15,
                    onTap: (_, latLng) {
                      tappedCoords = latLng;
                      Navigator.pop(context);
                      setState(() {
                        latlongController.text = latLng.latitude.toString() +
                            " " +
                            latLng.longitude.toString();
                      });
                    },
                  ),
                  children: [
                    TileLayer(
                      urlTemplate:
                          'https://tile.openstreetmap.org/{z}/{x}/{y}.png',
                      userAgentPackageName: 'com.example.app',
                    ),
                    if (tappedCoords != null)
                      MarkerLayer(
                        markers: [
                          Marker(
                            builder: (context) => const Icon(
                              Icons.circle,
                              size: 10,
                              color: Colors.black,
                            ),
                            width: pointSize,
                            height: pointSize,
                            point: tappedCoords!,
                          )
                        ],
                      ),
                  ],
                ),
                if (tappedPoint != null)
                  Positioned(
                    left: tappedPoint!.x - 60 / 2,
                    top: tappedPoint!.y - 60 / 2,
                    child: const IgnorePointer(
                      child: Icon(
                        Icons.center_focus_strong_outlined,
                        color: Colors.black,
                        size: 60,
                      ),
                    ),
                  )
              ],
            ),
          );
        });
  }
}
