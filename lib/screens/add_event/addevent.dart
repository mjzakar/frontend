import 'package:flutter/material.dart';
import 'package:frontend/screens/add_event/components/body.dart';

class AddEvent extends StatefulWidget {
  const AddEvent({super.key});

  @override
  State<AddEvent> createState() => _AdminPageState();
}

class _AdminPageState extends State<AddEvent> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Hello"),
      ),
    
      resizeToAvoidBottomInset: false,
      body: Container(
          decoration: BoxDecoration(
              gradient: LinearGradient(
                  colors: [Color.fromRGBO(37, 61, 100, 1), Color(0xFF191919)],
                  begin: Alignment.topLeft,
                  end: Alignment.bottomRight)),
          child: Body()),
    );
  }
}
