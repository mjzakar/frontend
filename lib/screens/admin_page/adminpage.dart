import 'package:flutter/material.dart';
import 'package:frontend/screens/add_event/addevent.dart';
import 'package:frontend/screens/admin_page/components/body.dart';
import 'package:frontend/screens/login/login.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AdminPage extends StatefulWidget {
  const AdminPage({super.key});

  @override
  State<AdminPage> createState() => _AdminPageState();
}

class _AdminPageState extends State<AdminPage> {
  void logOut() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setBool('loggedIn', false);
    prefs.remove('userId');
    prefs.remove('access');
    prefs.remove('refresh');
    Navigator.pushReplacement(
        context,
        MaterialPageRoute(
          builder: (context) => Login(),
        ));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(backgroundColor: Color(0xff393E46),
        actions: [
          IconButton(onPressed: logOut, icon: Icon(Icons.exit_to_app_outlined, color: Color(0xffEEEEEE),))
        ],
        title: Text("Admin Panel", style: TextStyle(color: Color(0xffEEEEEE)),),
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Color(0xff00ADB5),
        onPressed: () {
          Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => AddEvent(),
              ));
        },
        child: Icon(Icons.add, color: Color(0xffEEEEEE)),
      ),
      resizeToAvoidBottomInset: false,
      body: Container(
          decoration: BoxDecoration(
              gradient: LinearGradient(
                  colors: [Color.fromRGBO(37, 61, 100, 1), Color(0xFF191919)],
                  begin: Alignment.topLeft,
                  end: Alignment.bottomRight)),
          child: Body()),
    );
  }
}
