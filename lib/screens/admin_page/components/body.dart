import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:frontend/data/api_service.dart';
import 'package:frontend/models/Event.dart';
import 'package:latlong2/latlong.dart';
import 'package:pull_to_refresh_flutter3/pull_to_refresh_flutter3.dart';

class Body extends StatefulWidget {
  const Body({super.key});

  @override
  State<Body> createState() => _BodyState();
}

class _BodyState extends State<Body> {
  List<Event> events = [];
  RefreshController _refreshController =
      RefreshController(initialRefresh: false);
  final TextEditingController nameController = TextEditingController();
  final TextEditingController dateController = TextEditingController();
  final TextEditingController timeController = TextEditingController();
  final TextEditingController latlongController = TextEditingController();
  final TextEditingController venueController = TextEditingController();
  final TextEditingController priceController = TextEditingController();
  static const double pointSize = 65;

  final mapController = MapController();

  LatLng? tappedCoords;
  Point<double>? tappedPoint;
  @override
  void initState() {
    super.initState();
    ApiService().getAllEvents().then((eventsList) {
      if (eventsList != null) {
        setState(() {
          events = eventsList;
        });
      } else {
      }
    });
  }

  void deleteEvent(int index) {
    setState(() {
      events.removeAt(index);
    });
  }

  void _onRefresh() async {
    ApiService().getAllEvents().then((eventsList) {
      if (eventsList != null) {
        setState(() {
          events = eventsList;
        });
      } else {
      }
    });
    await Future.delayed(Duration(milliseconds: 1000));
    _refreshController.refreshCompleted();
  }

  @override
  Widget build(BuildContext context) {
    return SmartRefresher(
      enablePullDown: true,
      header: WaterDropHeader(),
      controller: _refreshController,
      onRefresh: _onRefresh,
      child: ListView.builder(
          itemCount: events.length,
          itemBuilder: (context, index) {
            return GestureDetector(
              onTap: () {
                _showModal(context, events[index]);
              },
              child: Padding(
                padding: const EdgeInsets.all(6.0),
                child: Container(
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20),
                      color: Color(0xff00ADB5)),
                  child: ListTile(
                    trailing: IconButton(
                      icon: Icon(Icons.clear),
                      onPressed: () async {
                        await ApiService().deleteEvent(events[index].id ?? "");
                        deleteEvent(index);
                      },
                    ),
                    title: Text(events[index].name ?? ""),
                  ),
                ),
              ),
            );
          }),
    );
  }

  DateTime selectedDate = DateTime.now();
  TimeOfDay selectedTime = TimeOfDay.now();

  Future<void> _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
          context: context,
          initialDate: selectedDate,
          firstDate: DateTime(2000),
          lastDate: DateTime(2101),
        ) ??
        DateTime.now();
    ;

    if (picked != null && picked != selectedDate)
      setState(() {
        selectedDate = picked;
        dateController.text = picked.toLocal().toString().split(' ')[0];
      });
  }

  Future<void> _selectTime(BuildContext context) async {
    final TimeOfDay picked = await showTimePicker(
          context: context,
          initialTime: selectedTime,
        ) ??
        TimeOfDay.now();

    if (picked != null && picked != selectedTime)
      setState(() {
        selectedTime = picked;
        timeController.text =
            '${selectedTime.hour.toString().padLeft(2, '0')}:${selectedTime.minute.toString().padLeft(2, '0')}:00';
      });
  }

  void _showModal(BuildContext context, Event event) {
    nameController.text = event.name ?? "";
    dateController.text = event.date ?? "";
    timeController.text = event.time ?? "";
    latlongController.text = event.lat! + " " + event.lng!;
    venueController.text = event.venueName ?? "";
    priceController.text = event.price ?? "";
    Widget _buildTextField({
      TextEditingController? controller,
      String? labelText,
      IconData? icon,
      Function? onPressedIcon,
      bool readOnly = false,
    }) {
      return Padding(
        padding: const EdgeInsets.all(8.0),
        child: TextFormField(
          controller: controller,
          readOnly: readOnly,
          style: TextStyle(color: Color(0xFFEEEEEE)),
          decoration: InputDecoration(
            labelText: labelText,
            labelStyle: TextStyle(color: Color(0xFFEEEEEE)),
            filled: true,
            fillColor: Color(0xFF222831),
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(10.0),
            ),
            prefixIcon: Icon(icon, color: Color(0xFF00ADB5)),
            suffixIcon: IconButton(
              icon: Icon(Icons.edit, color: Color(0xFF00ADB5)),
              onPressed: onPressedIcon as void Function()?,
            ),
          ),
        ),
      );
    }

    void _showMap(BuildContext context) async {
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return Center(
              child: Stack(
                children: [
                  FlutterMap(
                    mapController: mapController,
                    options: MapOptions(
                      center: LatLng(36.561128, 52.680570),
                      zoom: 15,
                      onTap: (_, latLng) {
                        tappedCoords = latLng;
                        Navigator.pop(context);
                        setState(() {
                          latlongController.text = latLng.latitude.toString() +
                              " " +
                              latLng.longitude.toString();
                        });
                      },
                    ),
                    children: [
                      TileLayer(
                        urlTemplate:
                            'https://tile.openstreetmap.org/{z}/{x}/{y}.png',
                        userAgentPackageName: 'com.example.app',
                      ),
                      if (tappedCoords != null)
                        MarkerLayer(
                          markers: [
                            Marker(
                              builder: (context) => const Icon(
                                Icons.circle,
                                size: 10,
                                color: Colors.black,
                              ),
                              width: pointSize,
                              height: pointSize,
                              point: tappedCoords!,
                            )
                          ],
                        ),
                    ],
                  ),
                  if (tappedPoint != null)
                    Positioned(
                      left: tappedPoint!.x - 60 / 2,
                      top: tappedPoint!.y - 60 / 2,
                      child: const IgnorePointer(
                        child: Icon(
                          Icons.center_focus_strong_outlined,
                          color: Colors.black,
                          size: 60,
                        ),
                      ),
                    )
                ],
              ),
            );
          });
    }

    showDialog(
      context: context,
      builder: (BuildContext context) {
        return Material(
          type: MaterialType.transparency,
          child: Container(
            width: 350,
            height: 500,
            padding: EdgeInsets.all(20),
            decoration: BoxDecoration(
              color: Color(0xFF222831),
              borderRadius: BorderRadius.circular(10),
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                _buildTextField(
                  controller: nameController,
                  labelText: 'Event Name',
                  icon: Icons.event,
                ),
                _buildTextField(
                  controller: dateController,
                  labelText: 'Date',
                  icon: Icons.calendar_today,
                  onPressedIcon: () => _selectDate(context),
                  readOnly: true,
                ),
                _buildTextField(
                  controller: timeController,
                  labelText: 'Time',
                  icon: Icons.access_time,
                  onPressedIcon: () => _selectTime(context),
                  readOnly: true,
                ),
                _buildTextField(
                  controller: latlongController,
                  labelText: 'Coordinates',
                  icon: Icons.pin_drop,
                  onPressedIcon: () => _showMap(context),
                ),
                _buildTextField(
                  controller: venueController,
                  labelText: 'Venue Name',
                  icon: Icons.place,
                ),
                _buildTextField(
                  controller: priceController,
                  labelText: 'Price',
                  icon: Icons.attach_money,
                ),
                SizedBox(height: 20),
                ElevatedButton(
                  onPressed: () async {
                    var newEvent = Event(
                      date: dateController.text,
                      lat: latlongController.text.split(" ")[0],
                      lng: latlongController.text.split(" ")[1],
                      name: nameController.text,
                      price: priceController.text,
                      time: timeController.text,
                      venueName: venueController.text,
                    );
                    print(newEvent.toJson());
                    await ApiService().updateEvent(event.id ?? "", newEvent);
                    Navigator.pop(context);
                  },
                  child: Text('Save Event',
                      style: TextStyle(color: Color(0xFFEEEEEE))),
                  style: ButtonStyle(
                    backgroundColor:
                        MaterialStateProperty.all<Color>(Color(0xFF00ADB5)),
                    padding: MaterialStateProperty.all<EdgeInsetsGeometry>(
                        EdgeInsets.all(10)),
                    shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                      RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20.0),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}
