// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:flutter/material.dart';
import 'package:frontend/screens/home_page/home_page.dart';
import 'package:frontend/screens/map_page/components/body.dart';

class MapPage extends StatelessWidget {
  MapPage({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
  appBar: AppBar(
    backgroundColor: Color(0xff00ADB5), 
    title: Text("View Events", style: TextStyle(color: Color(0xffEEEEEE)),),
    actions: [
      Container(
        margin: EdgeInsets.all(10.0), 
        decoration:
            BoxDecoration(shape: BoxShape.circle, color: Color(0xffEEEEEE)),
        child: IconButton(
          icon: Icon(
            Icons.person_2_outlined,
            color: Colors.grey.shade800,
          ),
          onPressed: () {
            Navigator.pushReplacement(context,
                MaterialPageRoute(builder: ((context) => HomePage())));
          
          },
        ),
      ),
    ],
  ),
      resizeToAvoidBottomInset: false,
      body: Container(
          decoration: BoxDecoration(
              gradient: LinearGradient(
                  colors: [Color.fromRGBO(37, 61, 100, 1), Color(0xFF191919)],
                  begin: Alignment.topLeft,
                  end: Alignment.bottomRight)),
          child: Body()),
    );
  }
}
