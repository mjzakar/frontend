import 'package:flutter_map/flutter_map.dart';
import 'package:frontend/models/Event.dart';
import 'package:frontend/models/Ticket.dart';
import 'package:frontend/screens/bank_page/bankpage.dart';
import 'package:latlong2/latlong.dart' as Lat;
import 'package:flutter/material.dart';
import 'package:frontend/data/api_service.dart' as API;
import 'package:ticket_widget/ticket_widget.dart';
class TeardropClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    Path path = Path();
    path.addOval(Rect.fromCircle(center: Offset(size.width / 2, size.height * 0.45), radius: size.width * 0.5));
    path.addPath(_createBottomTeardropPath(size), Offset.zero);
    return path;
  }

 Path _createBottomTeardropPath(Size size) {
    Path teardrop = Path();
    teardrop.moveTo(size.width / 2, size.height); 
    teardrop.lineTo(0, size.height / 2);
    teardrop.lineTo(size.width, size.height / 2); 
    teardrop.close();
    return teardrop;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) => false;
}

class Body extends StatefulWidget {
  Body({
    Key? key,
  }) : super(key: key);

  @override
  State<Body> createState() => _BodyState();
}

class _BodyState extends State<Body> {
  List<Event> events = [];

  @override
  void initState() {
    loadEvents();
    super.initState();
  }

  void loadEvents() async {
    List<Event> loadedEvents = await API.ApiService().getAllEvents() ?? [];
    setState(() {
      events = loadedEvents;
    });
  }

  void _showModal(BuildContext context, Event event) {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return Center(
            child: TicketWidget(
              width: 370,
              height: 500,
              isCornerRounded: true,
              padding: EdgeInsets.all(20),
              color: Color(0xFF393E46),
              child: EventDetails(event: event),
            ),
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return FlutterMap(
      options: MapOptions(
        center: Lat.LatLng(36.561128, 52.680570),
        zoom: 15,
      ),
      nonRotatedChildren: [
        MarkerLayer(
          markers: events.map((event) {
            return Marker(
              builder: (context) => InkWell(
                onTap: () {
                  _showModal(context, event);
                },
                child: ClipPath(
                  clipper: TeardropClipper(),
                  child: Container(
                    decoration: BoxDecoration(
                      color: Color(0xff00ADB5),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.grey.withOpacity(0.5),
                          spreadRadius: 10,
                          blurRadius: 15,
                          offset: Offset(0, 3),
                        ),
                      ],
                    ),
                    child: Container(
                      margin: EdgeInsets.all(1),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            event.name ?? "",
                            style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          
                        ],
                      ),
                    ),
                  ),
                ),
              ),
              point: Lat.LatLng(
                double.parse(event.lat ?? ""),
                double.parse(event.lng ?? ""),
              ),
              width: 100,
              height: 180,
            );
          }).toList(),
        ),
      ],
      children: [
        TileLayer(
          urlTemplate: 'https://tile.openstreetmap.org/{z}/{x}/{y}.png',
          userAgentPackageName: 'com.mojay.app',
        ),
        RichAttributionWidget(
          attributions: [
            TextSourceAttribution(
              'OpenStreetMap contributors',
              onTap: () {},
            ),
          ],
        ),
      ],
    );
  }
}

class EventDetails extends StatelessWidget {
  final Event event;

  EventDetails({required this.event});

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Container(
          decoration: BoxDecoration(
            color: Color(0xFF00ADB5),
            borderRadius: BorderRadius.all(
                Radius.circular(15.0) 
                ),
          ),
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 50, vertical: 15),
            child: Text(
              event.name!,
              style: TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.bold,
                  color: Color(0xFFEEEEEE)),
            ),
          ),
        ),
        SizedBox(height: 10),
        Container(
          margin: EdgeInsets.all(10),
          decoration: BoxDecoration(
            border: Border.all(width: 3.0, color: Color(0xff00ADB5)),
            borderRadius: BorderRadius.all(
                Radius.circular(20.0) 
                ),
          ),
          child: Padding(
            padding: const EdgeInsets.all(20.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    const Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Text(
                          'Venue:',
                          style:
                              TextStyle(fontSize: 17, color: Color(0xFFEEEEEE)),
                        ),
                        SizedBox(height: 10),
                        Text(
                          'Date:',
                          style:
                              TextStyle(fontSize: 17, color: Color(0xFFEEEEEE)),
                        ),
                        SizedBox(height: 10),
                        Text(
                          'Time: ',
                          style:
                              TextStyle(fontSize: 17, color: Color(0xFFEEEEEE)),
                        ),
                      ],
                    ),
                    SizedBox(
                      width: 40,
                    ),
                    Column(
                      children: [
                        Text(
                          '${event.venueName}',
                          style:
                              TextStyle(fontSize: 17, color: Color(0xFFEEEEEE)),
                        ),
                        SizedBox(height: 10),
                        Text(
                          '${event.date}',
                          style:
                              TextStyle(fontSize: 17, color: Color(0xFFEEEEEE)),
                        ),
                        SizedBox(height: 10),
                        Text(
                          '${event.time!.substring(0, 5)}',
                          style:
                              TextStyle(fontSize: 17, color: Color(0xFFEEEEEE)),
                        ),
                      ],
                    )
                  ],
                )
              ],
            ),
          ),
        ),
        SizedBox(height: 20),
        ElevatedButton(
          style: ElevatedButton.styleFrom(elevation: 20,
            backgroundColor: Color(0xff00ADB5)),
          onPressed: () {
            Navigator.pushReplacement(
                context,
                MaterialPageRoute(
                    builder: ((context) => BankPage(
                          event: event,
                        ))));
          },
          child: Padding(
            padding: const EdgeInsets.all(12.0),
            child: Text("Buy (\$${event.price})",
                style: TextStyle(fontSize: 17, color: Color(0xFFEEEEEE))),
          ),
        )
      ],
    );
  }
}
