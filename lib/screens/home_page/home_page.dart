// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:flutter/material.dart';
import 'package:frontend/data/api_service.dart';
import 'package:frontend/models/LoginDTO.dart';

import 'package:frontend/models/user.dart';
import 'package:frontend/screens/home_page/components/body.dart';
import 'package:frontend/screens/login/login.dart';
import 'package:frontend/screens/map_page/map.dart';
import 'package:jwt_decoder/jwt_decoder.dart';
import 'package:shared_preferences/shared_preferences.dart';

class HomePage extends StatefulWidget {
  HomePage({
    Key? key,
  }) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  void refreshTokenInvalid() {
  }
  void logOut() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setBool('loggedIn', false);
    prefs.remove('userId');
    prefs.remove('access');
    prefs.remove('refresh');
    Navigator.pushReplacement(
        context,
        MaterialPageRoute(
          builder: (context) => Login(),
        ));
  }

  User? user;

  Future<void> initialize() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? access = prefs.getString('access');
    String? refresh = prefs.getString('refresh');
    String? userId = prefs.getString('userId');
    if (access != null && refresh != null && userId != null) {
      if (JwtDecoder.isExpired(access)) {
        if (!JwtDecoder.isExpired(refresh)) {
          LoginDTO? loginDetails = await ApiService().refresh(refresh);
          if (loginDetails == null)
            logOut();
          else {
            prefs.setBool('loggedIn', true);
            prefs.setString('userId', loginDetails.userId);
            prefs.setString('access', loginDetails.accessToken);
            prefs.setString('refresh', loginDetails.refreshToken);
            user = await ApiService()
                .getUserDetails(loginDetails.accessToken, loginDetails.userId);
          }
        } else {
          logOut();
        }
      } else {
        user = await ApiService().getUserDetails(access, userId);
      }
    }
    if (user == null) logOut();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("User Profile", style: TextStyle(color: Color(0xffEEEEEE))),
          backgroundColor: Color(0xff00ADB5),
          leading: IconButton(icon: Icon(Icons.arrow_back), onPressed: () =>  Navigator.pushReplacement(
                              context,
                              MaterialPageRoute(
                                  builder: ((context) => MapPage()))),),
          actions: [
            IconButton(
                onPressed: () {
                  logOut();
                },
                icon: Icon(Icons.exit_to_app_outlined, color: Color(0xffEEEEEE),))
          ],
        ),
        body: Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height ,
            decoration: BoxDecoration(
                gradient: LinearGradient(
                    colors: [Color.fromRGBO(37, 61, 100, 1), Color(0xFF191919)],
                    begin: Alignment.topLeft,
                    end: Alignment.bottomRight)),
            child: FutureBuilder(
                future: initialize(),
                builder: (context, snapshot) {
                  switch (snapshot.connectionState) {
                    case ConnectionState.waiting:
                      return CircularProgressIndicator();
                    default:
                      return Body(user: user!);
                  }
                })));
      }
}

