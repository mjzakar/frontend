// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:flutter/material.dart';
import 'package:frontend/data/api_service.dart' as API;
import 'package:frontend/models/Ticket.dart';
import 'package:frontend/models/user.dart';
import 'package:qr_flutter/qr_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:ticket_widget/ticket_widget.dart';

class Body extends StatefulWidget {
  User user;
  Body({
    Key? key,
    required this.user,
  }) : super(key: key);

  @override
  State<Body> createState() => _BodyState();
}

class _BodyState extends State<Body> {
  String userId = "";
  List<Ticket> userTickets = [];

  @override
  void initState() {
    loadUserId();
    super.initState();
  }

  void loadUserId() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? storedUserId = prefs.getString('userId');
    print(storedUserId);

    if (storedUserId != null) {
      setState(() {
        userId = storedUserId;
        loadUserTickets();
      });
    }
  }

  void loadUserTickets() async {
    List<Ticket> tickets =
        await API.ApiService().getAllTicketsForUser(userId) ?? [];
    print(tickets);

    setState(() {
      userTickets = tickets;
    });
  }

  @override
  Widget build(BuildContext context) {
    return userTickets.isEmpty
        ? Center(
            child: Text('No tickets available'),
          )
        : Padding(
  padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
  child: Container(
    decoration: BoxDecoration(
      color: Color(0xff393E46), 
      borderRadius: BorderRadius.circular(20),
      boxShadow: [
        BoxShadow(
          color: Colors.black.withOpacity(0.2),
          spreadRadius: 1,
          blurRadius: 7,
          offset: Offset(0, 3),
        ),
      ],
    ),
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Padding(
          padding: const EdgeInsets.all(20.0),
          child: Text(
            "Your Tickets",
            style: TextStyle(fontSize: 24, color: Colors.white, fontWeight: FontWeight.bold),
            textAlign: TextAlign.center,
          ),
        ),
        Divider(color: Colors.white70),
        SizedBox(height: 20,),
        Expanded(
          child: ListView.builder(
            itemCount: userTickets.length,
            itemBuilder: (context, index) {
              Ticket ticket = userTickets[index];
              return Padding(
                padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                child: Container(
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(30),
                      color: Color(0xff00ADB5),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.black.withOpacity(0.2),
                          spreadRadius: 1,
                          blurRadius: 7,
                          offset: Offset(0, 3),
                        ),
                      ],
                  ),
                  child: ListTile(
                    title: Text(
                      ticket.name ?? "",
                      style: TextStyle(fontSize: 17, color: Colors.white),
                    ),
                    subtitle: Text(
                      '${ticket.date}',
                      style: TextStyle(color: Color(0xffEEEEEE)),
                    ),
                    onTap: () {
                      _showModal(context, ticket);
                    },
                  ),
                ),
              );
            },
          ),
        ),
      ],
    ),
  ),
);
  }

  void _showModal(BuildContext context, Ticket ticket) {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return Center(
            child: TicketWidget(
              width: 350,
              height: 500,
              isCornerRounded: true,
              padding: EdgeInsets.all(20),
              color: Colors.white,
              child: TicketDetails(ticket: ticket),
            ),
          );
        });
  }
}

class TicketDetails extends StatelessWidget {
  final Ticket ticket;

  TicketDetails({required this.ticket});

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Text(
          ticket.name!,
          style: TextStyle(
              fontSize: 22, fontWeight: FontWeight.bold, color: Colors.black),
        ),
        SizedBox(height: 20),
        Text(
          '${ticket.firstName} ${ticket.lastName}',
          style: TextStyle(fontSize: 18, color: Colors.black),
        ),
        SizedBox(height: 20),
        Text(
          'Venue: ${ticket.venueName}',
          style: TextStyle(fontSize: 16, color: Colors.black),
        ),
        Text(
          'Date and Time: ${ticket.date} ${ticket.time!.substring(0, 5)}',
          style: TextStyle(fontSize: 16, color: Colors.black),
        ),
        SizedBox(height: 30),
        QrImageView(
          data: ticket.id ?? "",
          version: QrVersions.auto,
          size: 250.0,
        ),
      ],
    );
  }
}
