import 'dart:convert';

import 'package:frontend/models/Event.dart';
import 'package:frontend/models/Ticket.dart';
import 'package:http/http.dart' as http;
import 'package:frontend/models/LoginDTO.dart';
import 'package:frontend/models/user.dart';

class ApiService {
  final authPath = 'http://10.0.2.2:8080/api/auth';
  final userPath = 'http://10.0.2.2:8080/api/user';

  Future<LoginDTO?> login(String email, String password) async {
    final response = await http.post(Uri.parse('$authPath/login'),
        body: json.encode({'email': email, 'password': password}),
        headers: {'Content-Type': 'application/json', 'Accept': '*/*'});
    if (response.statusCode == 200) {
      return LoginDTO.fromJson(response.body);
    } else
      return null;
  }

  Future<LoginDTO?> signup(User user) async {
    final response = await http.post(
      Uri.parse('$authPath/signup'),
      headers: {'Content-Type': 'application/json', 'Accept': '*/*'},
      body: jsonEncode(user.toJson()),
    );

    if (response.statusCode == 200) {
      return LoginDTO.fromJson(response.body);
    } else if (response.statusCode == 409)
      return null;
    else
      return null;
  }

  Future<LoginDTO?> refresh(String refresh) async {
    final response = await http.post(Uri.parse('$authPath/refresh'),
        headers: {'Content-Type': 'application/json', 'Accept': '*/*'},
        body: json.encode({'refreshToken': refresh}));

    if (response.statusCode == 200) {
      return LoginDTO.fromJson(response.body);
    } else if (response.statusCode == 409)
      return null;
    else
      return null;
  }

  Future<User?> getUserDetails(String accessToken, String userId) async {
    final response = await http.post(Uri.parse('$userPath/get'),
        headers: {'Content-Type': 'application/json', 'Accept': '*/*'},
        body: json.encode({'accessToken': accessToken, 'userId': userId}));
    if (response.statusCode == 200) {
      return User.fromJson(jsonDecode(response.body));
    } else if (response.statusCode == 409)
      return null;
    else
      return null;
  }

  Future<Ticket?> addTicket(Ticket ticket) async {
    final String apiUrl = 'http://10.0.2.2:8082/tickets';

    final response = await http.post(
      Uri.parse(apiUrl),
      headers: {'Content-Type': 'application/json', 'Accept': '*/*'},
      body: jsonEncode(ticket.toJson()),
    );
    print(response.statusCode);
    if (response.statusCode == 201) {
      final Map<String, dynamic> responseData = json.decode(response.body);
      return Ticket.fromJson(responseData);
    } else {
      return null;
    }
  }

  Future<List<Ticket>?> getAllTicketsForUser(String userId) async {
    final String apiUrl = 'http://10.0.2.2:8082/tickets/user/$userId';

    final response = await http.get(
      Uri.parse(apiUrl),
      headers: {'Content-Type': 'application/json', 'Accept': '*/*'},
    );
    if (response.statusCode == 200) {
      final List<dynamic> responseData = json.decode(response.body);
      final List<Ticket> userTickets =
          responseData.map((json) => Ticket.fromJson(json)).toList();
      return userTickets;
    } else if (response.statusCode == 404) {
      return [];
    } else {
      print(
          'Failed to get tickets for user. Status code: ${response.statusCode}');
      return null;
    }
  }

  Future<Ticket?> getTicketById(String ticketId) async {
    final String apiUrl = 'http://10.0.2.2:8082/$ticketId';

    final response = await http.get(
      Uri.parse(apiUrl),
      headers: {'Content-Type': 'application/json', 'Accept': '*/*'},
    );

    if (response.statusCode == 200) {
      final Map<String, dynamic> responseData = json.decode(response.body);
      return Ticket.fromJson(responseData);
    } else if (response.statusCode == 404) {
      return null;
    } else {
      print('Failed to get ticket by id. Status code: ${response.statusCode}');
      return null;
    }
  }

  Future<List<Event>?> getAllEvents() async {
    final String apiUrl = 'http://10.0.2.2:8081/events';

    final response = await http.get(
      Uri.parse(apiUrl),
      headers: {'Content-Type': 'application/json', 'Accept': '*/*'},
    );
    print(response.statusCode);
    if (response.statusCode == 200) {
      final List<dynamic> responseData = json.decode(response.body);
      final List<Event> events =
          responseData.map((json) => Event.fromJson(json)).toList();
      return events;
    } else {
      return null;
    }
  }

  Future<Event?> getEventById(String eventId) async {
    final String apiUrl = 'http://10.0.2.2:8081/events/$eventId';

    final response = await http.get(
      Uri.parse(apiUrl),
      headers: {'Content-Type': 'application/json', 'Accept': '*/*'},
    );

    if (response.statusCode == 200) {
      final Map<String, dynamic> responseData = json.decode(response.body);
      return Event.fromJson(responseData);
    } else if (response.statusCode == 404) {
      return null;
    } else {
      return null;
    }
  }

  Future<Event?> createEvent(Event event) async {
    final String apiUrl =
        'http://10.0.2.2:8081/events';

    final response = await http.post(
      Uri.parse(apiUrl),
      headers: {'Content-Type': 'application/json', 'Accept': '*/*'},
      body: jsonEncode(event.toJson()),
    );
    print(response.statusCode);
    if (response.statusCode == 201) {
      final dynamic responseData = json.decode(response.body);
      return Event.fromJson(responseData);
    } else {

      return null;
    }
  }

  Future<bool> deleteEvent(String eventId) async {
    final String apiUrl =
        'http://10.0.2.2:8081/events/$eventId'; 

    final response = await http.delete(
      Uri.parse(apiUrl),
      headers: {'Content-Type': 'application/json', 'Accept': '*/*'},
    );

    if (response.statusCode == 204) {
      return true;
    } else if (response.statusCode == 404) {
      return false;
    } else {
      return false;
    }
  }

  Future<bool> updateEvent(String eventId, Event updatedEvent) async {
    final String apiUrl =
        'http://10.0.2.2:8081/events/$eventId'; 

    final response = await http.put(
      Uri.parse(apiUrl),
      headers: {'Content-Type': 'application/json', 'Accept': '*/*'},
      body: jsonEncode(updatedEvent.toJson()),
    );
    print(response.statusCode);
    if (response.statusCode == 200) {

      return true;
    } else if (response.statusCode == 404) {
      return false;
    } else {
      return false;
    }
  }
}
